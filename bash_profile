# Set locale for UTF-8
LANG=en_US.utf8

# Add keys to be tracked by keychain
# TODO Consider greping all pub files to find id files? This only finds rsa
eval `keychain -q --noask --eval ~/.ssh/*id_rsa`

# Source the bashrc
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi
