# Set locale for UTF-8
LANG=en_US.utf8

# Add keys to be tracked by keychain
# TODO Consider greping all pub files to find id files? This only finds rsa
eval `keychain -q --noask --eval ~/.ssh/*id_rsa`
eval `keychain -q --noask --eval ~/.ssh/id_ed25519`
