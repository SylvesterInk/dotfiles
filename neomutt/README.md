# Setup
## Required Packages
isync       (receive/sync imap)
msmtp       (send smtp emails)
neomutt     (manage emails)
notmuch     (tagging and searching emails)
urlscan     (opening urls in browser)
abook       (address book)
w3m         (viewing html emails in terminal)
sxiv        (viewing images, current choice in mailcap)

## Local Configs
Use the files in "./local.templates/" to create account configs for
isync, msmtprc, and mutt.

## Passwords
Create encrypted password files for access by the accounts.

## GPG-Agent
To increase the cache time, create a '~/.gnupg/gpg-agent.conf'
Add 'max-cache-ttl [NUMBER OF SECONDS]' to increase max cache time, so gpg key
doesn't expire too quickly for email check script.  (The max time is 2 hours by
default, as long as the key is used every 10 minutes.  We generally check every 5.)

## Auto Synchronization
Make sure the get_mail script in "./scripts/" is set up correctly and
add a cron entry to crontab.  This one will synchronize every 5 minutes
*/5 * * * * ~/dotfiles/scripts/get_mail
